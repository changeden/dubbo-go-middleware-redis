module gitee.com/changeden/dubbo-go-middleware-redis

go 1.16

require (
	dubbo.apache.org/dubbo-go/v3 v3.0.1
	gitee.com/changeden/dubbo-go-starter v0.1.4
	github.com/go-redis/redis/v8 v8.11.4
)
